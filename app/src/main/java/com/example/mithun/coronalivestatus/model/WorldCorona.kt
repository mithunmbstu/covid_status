package com.example.mithun.coronalivestatus.model

data class WorldCorona(
    val created: String,
    val totalActiveCases: Int,
    val totalCasesPerMillionPop: Int,
    val totalConfirmed: Int,
    val totalDeaths: Int,
    val totalNewCases: Int,
    val totalNewDeaths: Int,
    val totalRecovered: Int
)