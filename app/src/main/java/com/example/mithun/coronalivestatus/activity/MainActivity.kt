package com.example.mithun.coronalivestatus.activity

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.location.*
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.ViewPager
import com.example.mithun.coronalivestatus.R
import com.example.mithun.coronalivestatus.adapter.PoiAdapter
import com.example.mithun.coronalivestatus.adapter.ViewPagerAdapter
import com.example.mithun.coronalivestatus.fragment.OwnCountry
import com.google.android.material.tabs.TabLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), MultiplePermissionsListener,
    OwnCountry.OnHeadlineSelectedListener {
    var tabLayout: TabLayout? = null
    lateinit var liMain: LinearLayout
    var viewPager: ViewPager? = null
    var context: Context? = null
    private var latitude: Double? = null
    private var longitude: Double? = null
    var address: String? = null
    private var locationManager: LocationManager? = null
    var countryName: String? = null
    private lateinit var tvTittle: TextView
    internal lateinit var viewpageradapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        initialize()
    }

    private fun initialize() {
        context = this
        liMain = findViewById(R.id.liMain)
        tvTittle=findViewById(R.id.tvTitle)
        liMain.setBackgroundColor(Color.parseColor("#670000"))
        permmisionAll()
        tabLayout = findViewById(R.id.tab_layout)
        viewPager = findViewById(R.id.viewPager)
        viewpageradapter = ViewPagerAdapter(supportFragmentManager)
        this.viewPager?.adapter = viewpageradapter
        this.tab_layout.setupWithViewPager(this.viewPager)
        val typeface = Typeface.createFromAsset(applicationContext.assets, "font/Roboto-BoldCondensed.ttf")
        tvTittle.setTypeface(typeface)

    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {

    }

    override fun onPermissionRationaleShouldBeShown(
        permissions: MutableList<PermissionRequest>?, token: PermissionToken?
    ) {
    }

    fun permmisionAll() {
        val permissions = listOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        Dexter.withActivity(this).withPermissions(permissions).withListener(this)
            .check()
    }

    override fun onArticleSelected(country: String) {
        countryName = country
        tabLayout?.getTabAt(0)?.setText(countryName);
    }


}












