package com.example.mithun.coronalivestatus.activity

import android.Manifest
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mithun.coronalivestatus.R
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener


class SplashActivity : AppCompatActivity(), MultiplePermissionsListener {
    lateinit var linearLayout: LinearLayout
    var check: Boolean = false
    val PERMISSIONS_REQUEST: Int = 111
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()
        //making this activity full screen
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)
        linearLayout = findViewById(R.id.liSpash)
        linearLayout.setBackgroundResource(R.drawable.splash)
        if (Build.VERSION.SDK_INT >= 23) {
            permmisionAll()
        } else {
            startNextActivity();
        }
    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        if (report?.areAllPermissionsGranted()!!){
            startNextActivity();
        }
        else if (report.isAnyPermissionPermanentlyDenied){

            startNextActivity();
        }

    }

    override fun onPermissionRationaleShouldBeShown(
        permissions: MutableList<PermissionRequest>?, token: PermissionToken?
    ) {

    }

    fun permmisionAll() {
        val permissions = listOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        Dexter.withActivity(this).withPermissions(permissions).withListener(this)
            .check()

    }

    private fun startNextActivity() {
        Handler().postDelayed({
            //start main activity
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            //finish this activity
            finish()
        }, 1000)
    }


}