package com.example.mithun.coronalivestatus.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.mithun.coronalivestatus.fragment.OwnCountry
import com.example.mithun.coronalivestatus.fragment.WorldData

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        if (position == 0) {
            fragment = OwnCountry()
        } else if (position == 1) {
            fragment = WorldData()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = "Own Country"


        } else if (position == 1) {
            title = "World "
        }
        return title
    }
}