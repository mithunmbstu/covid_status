package com.example.mithun.coronalivestatus.fragment

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.location.*
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.juprojectkotlin.server.DestinationService
import com.example.juprojectkotlin.server.ServiceBuilder
import com.example.mithun.coronalivestatus.R
import com.example.mithun.coronalivestatus.adapter.PoiAdapter
import com.example.mithun.coronalivestatus.model.CoronaItem
import com.example.mithun.coronalivestatus.util.KeyboardUtil
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.roger.catloadinglibrary.CatLoadingView
import dmax.dialog.SpotsDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class OwnCountry : Fragment() {
    private var loaderView: CatLoadingView? = null
    private var mContext: Context? = null
    private lateinit var tvTodayDectection: TextView
    private lateinit var tvTodayDeath: TextView
    private lateinit var tvTodayActiveCase: TextView
    private lateinit var tvTitleTodayDectection: TextView
    private lateinit var tvTitleTodayDeath: TextView
    private lateinit var tvTitleTodayActiveCase: TextView
    private lateinit var tvTotalDeath: TextView
    private lateinit var tvToalRecovered: TextView
    private lateinit var tvTotalConfirmed: TextView
    private lateinit var tvLastTweenty: TextView
    private lateinit var tvTitleTotalConfirm: TextView
    private lateinit var tvTitleOwnTotalDeath: TextView
    private lateinit var tvTitleOwnTotalRecovered: TextView
    private lateinit var tvChartTitle: TextView
    private lateinit var tvOwnTotal: TextView
    private lateinit var autoCompleteTextView: AutoCompleteTextView
    private var acSearchName: String? = null
    private var latitude: Double? = null
    private var longitude: Double? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    lateinit var imgSearch: ImageView
    private var locationManager: LocationManager? = null
    private var addressCountry: String? = null
    var coronaList: List<CoronaItem>? = null
    var totalConfirm: Float? = null
    var totalDeath: Float? = null
    var totalActive: Float? = null
    var mCallback: OnHeadlineSelectedListener? = null
    var dialog: AlertDialog? = null
    lateinit var mChart: PieChart
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.own_country, container, false)
        initialize(view)


        return view
    }

    interface OnHeadlineSelectedListener {
        fun onArticleSelected(country: String)
    }

    private fun initialize(view: View) {
        mContext = requireActivity()
        loaderView = CatLoadingView()
        mChart = view.findViewById(R.id.pie_chart)
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(mContext as FragmentActivity)
        tvTitleTodayActiveCase = view.findViewById(R.id.tvTitleActive)
        tvTodayDectection = view.findViewById(R.id.tvDailyConfirm)
        tvTodayDeath = view.findViewById(R.id.tvTodayDeath)
        tvTodayActiveCase = view.findViewById(R.id.tvActiveCase)
        tvTotalDeath = view.findViewById(R.id.tvTotalDeath)
        tvToalRecovered = view.findViewById(R.id.tvTotalRecovered)
        tvTotalConfirmed = view.findViewById(R.id.tvTotalConfirm)
        autoCompleteTextView = view.findViewById(R.id.etSerach)
        imgSearch = view.findViewById(R.id.imgSeach)
        mChart?.setUsePercentValues(true)
        tvLastTweenty = view.findViewById(R.id.tvLastTweenty)
        tvTitleTodayDectection = view.findViewById(R.id.tvTitleDailyConfirm)
        tvTitleTodayDeath = view.findViewById(R.id.tvTitleTodayDeath)
        tvTitleTotalConfirm = view.findViewById(R.id.tvTitleOwnTotalConfirm)
        tvTitleOwnTotalDeath = view.findViewById(R.id.tvTitleOwnTotalDeath)
        tvTitleOwnTotalRecovered = view.findViewById(R.id.tvTitleOwnTotalRecovered)
        tvOwnTotal = view.findViewById(R.id.tvOwnTotal)
        tvChartTitle = view.findViewById(R.id.tvChartTitle)
        dialog = SpotsDialog.Builder().setContext(mContext).setMessage("Please wait....").build()
        if (isNetworkConnected()) {
            getCoronaList()
        } else {
            Toast.makeText(mContext, "Please Check Internet Connection", Toast.LENGTH_LONG).show()
        }

        locationManager =
            (mContext as FragmentActivity).getSystemService(LOCATION_SERVICE) as LocationManager?
        if (ActivityCompat.checkSelfPermission(
                mContext as FragmentActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mContext as FragmentActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        locationManager?.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0L,
            0f,
            locationListener
        )
        autoCompleteTextView.threshold = 2
        autoCompleteTextView.setOnItemClickListener() { parent, _, position, id ->
            val selectedPoi = parent.adapter.getItem(position) as CoronaItem?
            autoCompleteTextView?.setText(selectedPoi?.country)
            acSearchName = selectedPoi?.country

        }
        imgSearch.setOnClickListener {
            if (autoCompleteTextView?.text.length!! > 0) {
                KeyboardUtil.hideKeyboard(mContext as FragmentActivity)
                if (isNetworkConnected()) {
                    searchCountry(acSearchName!!)
                } else {
                    Toast.makeText(mContext, "Please Check Internet Connection", Toast.LENGTH_LONG)
                        .show()
                }

            }

        }
        addFontView()
    }

    private fun addFontView() {
        val tyTitle = Typeface.createFromAsset(mContext?.assets, "font/Roboto-Medium.ttf")
        val tyData = Typeface.createFromAsset(mContext?.assets, "font/Roboto-Regular.ttf")
        tvLastTweenty.setTypeface(tyTitle)
        tvTitleOwnTotalRecovered.setTypeface(tyTitle)
        tvTitleTodayDectection.setTypeface(tyTitle)
        tvTitleTodayDeath.setTypeface(tyTitle)
        tvTitleTotalConfirm.setTypeface(tyTitle)
        tvTitleOwnTotalDeath.setTypeface(tyTitle)
        tvOwnTotal.setTypeface(tyTitle)
        tvTitleTodayActiveCase.setTypeface(tyTitle)
        tvChartTitle.setTypeface(tyTitle)

        tvTodayDectection.setTypeface(tyData)
        tvTodayDeath.setTypeface(tyData)
        tvTodayActiveCase.setTypeface(tyData)
        tvTotalDeath.setTypeface(tyData)
        tvToalRecovered.setTypeface(tyData)
        tvTotalConfirmed.setTypeface(tyData)


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mCallback = activity as OnHeadlineSelectedListener
    }

    private fun getCoronaList() {
        dialog = SpotsDialog.Builder().setContext(mContext).setMessage("Please wait....").build()
        dialog?.show()
        val destinationService: DestinationService =
            ServiceBuilder.builService(DestinationService::class.java)
        val listCorona = destinationService.getInfo()
        listCorona?.enqueue(object : Callback<List<CoronaItem>> {
            override fun onFailure(call: Call<List<CoronaItem>>, t: Throwable) {
                Log.e("error", t.message)
                dialog?.dismiss()
            }

            override fun onResponse(
                call: Call<List<CoronaItem>>, response: Response<List<CoronaItem>>
            ) {
                coronaList = response.body()!!
                Log.e("main response", response.body().toString())


                for (i in 0..coronaList!!.size - 1) {
//                    Log.e("inside llop",addressCountry)
                    if (coronaList!!.get(i).country.equals(addressCountry)) {
                        Log.e("inside llop", addressCountry)
                        tvTodayActiveCase.setText("" + coronaList!!.get(i).activeCases)
                        tvTodayDectection.setText("" + coronaList!!.get(i).dailyConfirmed)
                        tvTodayDeath.setText("" + coronaList!!.get(i).dailyDeaths)
                        tvTotalConfirmed.setText("" + coronaList!!.get(i).totalConfirmed)
                        tvToalRecovered.setText("" + coronaList!!.get(i).totalRecovered)
                        tvTotalDeath.setText("" + coronaList!!.get(i).totalDeaths)
                        totalConfirm = coronaList!!.get(i).totalConfirmed.toFloat()
                        totalDeath = coronaList!!.get(i).totalDeaths.toFloat()
                        totalActive = coronaList!!.get(i).totalRecovered.toFloat()
                    }
                }

                mChart?.setUsePercentValues(true)
                val desc: Description = Description()
                desc.text = "% Rate Chart"
                mChart?.description = desc

                // val legend: Legend? = requireActivity().legend
                val legend: Legend? = mChart?.legend
                legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT

                val value = Arrays.asList(totalConfirm, totalDeath, totalActive)
                val label = Arrays.asList("Confirm", "Dead", "Recovered")

                val entry = ArrayList<PieEntry>()
                for (i in value.indices) {
                    value.get(i)?.let { PieEntry(it, label.get(i)) }?.let { entry.add(it) }

                }


                val dataSet = PieDataSet(entry, "")
                dataSet.setColors(Color.RED, Color.BLACK, Color.GREEN);
                dataSet.setDrawValues(true)
                //  dataSet.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);

                val pieData = PieData(dataSet)
                pieData.setValueFormatter(PercentFormatter())
                pieData.setValueTextSize(10f)
                pieData.setValueTextColor(Color.WHITE)

                mChart?.data = pieData

                dialog?.dismiss()

                val adapter =
                    PoiAdapter(requireContext(), android.R.layout.simple_list_item_1, coronaList!!)
                autoCompleteTextView.setAdapter(adapter)


            }
        })

    }

    private fun searchCountry(countryName: String) {
        dialog?.show()
        val destinationService: DestinationService =
            ServiceBuilder.builService(DestinationService::class.java)
        val listCorona = destinationService.getInfo()
        listCorona?.enqueue(object : Callback<List<CoronaItem>> {
            override fun onFailure(call: Call<List<CoronaItem>>, t: Throwable) {
                Log.e("error", t.message)
                dialog?.dismiss()
            }

            override fun onResponse(
                call: Call<List<CoronaItem>>, response: Response<List<CoronaItem>>
            ) {
                dialog?.dismiss()
                var coronaList: List<CoronaItem>? = response.body()!!
                for (i in 0..coronaList!!.size - 1) {
                    if (coronaList.get(i).country.equals(countryName)) {
                        tvTodayActiveCase.setText("" + coronaList.get(i).activeCases)
                        tvTodayDectection.setText("" + coronaList.get(i).dailyConfirmed)
                        tvTodayDeath.setText("" + coronaList.get(i).dailyDeaths)
                        tvTotalConfirmed.setText("" + coronaList.get(i).totalConfirmed)
                        tvToalRecovered.setText("" + coronaList.get(i).totalRecovered)
                        tvTotalDeath.setText("" + coronaList.get(i).totalDeaths)
                        totalConfirm = coronaList!!.get(i).totalConfirmed.toFloat()
                        totalDeath = coronaList!!.get(i).totalDeaths.toFloat()
                        totalActive = coronaList!!.get(i).totalRecovered.toFloat()
                        mCallback?.onArticleSelected(countryName!!)
                    }

                }
                dialog?.dismiss()
                mChart?.setUsePercentValues(true)
                val desc: Description = Description()
                desc.text = "% Rate Chart"
                mChart?.description = desc
                // val legend: Legend? = requireActivity().legend
                val legend: Legend? = mChart?.legend
                legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT

                val value = Arrays.asList(totalConfirm, totalDeath, totalActive)
                val label = Arrays.asList("Confirm", "Dead", "Recovered")

                val entry = ArrayList<PieEntry>()
                for (i in value.indices) {
                    value.get(i)?.let { PieEntry(it, label.get(i)) }?.let { entry.add(it) }

                }
                val dataSet = PieDataSet(entry, "")
                dataSet.setColors(Color.RED, Color.BLACK, Color.GREEN);
                dataSet.setDrawValues(true)
                //  dataSet.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);

                val pieData = PieData(dataSet)
                pieData.setValueFormatter(PercentFormatter())
                pieData.setValueTextSize(10f)
                pieData.setValueTextColor(Color.WHITE)
                mChart?.data = pieData
                dialog?.dismiss()

            }
        })

    }

    private fun getAddress() {
        try {
            val geo =
                Geocoder(mContext?.getApplicationContext(), Locale.getDefault())
            val addresses: List<Address> = longitude?.let {
                latitude?.let { it1 ->
                    geo.getFromLocation(
                        it1, it, 1
                    )
                }
            } as List<Address>
            if (addresses.isEmpty()) {
            } else {
                if (addresses.size > 0) {
                    addressCountry = addresses[0].countryName!!
                    if (addressCountry?.length!! > 0) {
                        searchCountry(addressCountry!!)
                        dialog?.dismiss()
                    }
                    Log.e("getaddress", addressCountry)
                    mCallback?.onArticleSelected(addressCountry!!)

                }
            }
        } catch (e: Exception) {
            e.printStackTrace() // getFromLocation() may sometimes fail
        }
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            latitude = location.latitude
            longitude = location.longitude
            Log.e("aaaa", location.longitude.toString())
            getAddress()
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    companion object {
        fun newInstance(): OwnCountry {
            val fragment = OwnCountry()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    private fun isNetworkConnected(): Boolean {
        val cm: ConnectivityManager =
            mContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()
    }

}