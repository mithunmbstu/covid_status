package com.example.mithun.coronalivestatus.model

import android.os.Parcel
import android.os.Parcelable

data class CoronaItem (
    val FR: String,
    val PR: String,
    val activeCases: Int,
    val country: String,
    val countryCode: String,
    val dailyConfirmed: Int,
    val dailyDeaths: Int,
    val lastUpdated: String,
    val lat: Double,
    val lng: Double,
    val totalConfirmed: Int,
    val totalConfirmedPerMillionPopulation: Int,
    val totalCritical: Int,
    val totalDeaths: Int,
    val totalDeathsPerMillionPopulation: Int,
    val totalRecovered: Int
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("Not yet implemented")
    }

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    companion object CREATOR : Parcelable.Creator<CoronaItem> {
        override fun createFromParcel(parcel: Parcel): CoronaItem {
            return CoronaItem(parcel)
        }

        override fun newArray(size: Int): Array<CoronaItem?> {
            return arrayOfNulls(size)
        }
    }
}