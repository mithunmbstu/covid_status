package com.example.juprojectkotlin.server


import com.example.mithun.coronalivestatus.model.CoronaItem
import com.example.mithun.coronalivestatus.model.WorldCorona
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

interface DestinationService {
    @Headers("Content-Type: application/json")
    @GET("country")
    fun getInfo(): Call<List<CoronaItem>>
    @Headers("Content-Type: application/json")
    @GET("global")
    fun getWorldData(): Call<WorldCorona>
}