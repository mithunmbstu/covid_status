package com.example.mithun.coronalivestatus.adapter

import android.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.example.mithun.coronalivestatus.model.CoronaItem


public class PoiAdapter(context: Context, @LayoutRes private val layoutResource: Int, private val allPois: List<CoronaItem>):
ArrayAdapter<CoronaItem>(context, layoutResource, allPois),
    Filterable {
    private var mPois: List<CoronaItem> = allPois

    override fun getCount(): Int {
        return mPois.size
    }

    override fun getItem(p0: Int): CoronaItem? {
        return mPois.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        // Or just return p0
        return mPois.size.toLong()
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.text =mPois.get(position).country
        return view
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
                mPois = filterResults.values as List<CoronaItem>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase()

                val filterResults = Filter.FilterResults()
                filterResults.values = if (queryString==null || queryString.isEmpty())
                    allPois
                else
                    allPois.filter {
                        it.country.toLowerCase().contains(queryString)
                    }
                return filterResults
            }
        }
    }
}