package com.example.mithun.coronalivestatus.util

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.inputmethod.InputMethodManager


object KeyboardUtil {
    fun hideKeyboard(activity: Activity) {
        try {
            val inputManager: InputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                activity.currentFocus.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        } catch (e: Exception) {
            // Ignore exceptions if any
            Log.e("KeyBoardUtil", e.toString(), e)
        }
    }
}