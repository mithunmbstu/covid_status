package com.example.mithun.coronalivestatus.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.juprojectkotlin.server.DestinationService
import com.example.juprojectkotlin.server.ServiceBuilder
import com.example.mithun.coronalivestatus.R
import com.example.mithun.coronalivestatus.model.WorldCorona
import com.example.mithun.coronalivestatus.util.InternetConnctionCheck
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import kotlinx.android.synthetic.main.world_stat.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class WorldData : Fragment() {
    lateinit var tvTotalConfirmed: TextView
    lateinit var tvTotalDeath: TextView
    lateinit var tvTotalRecovred: TextView
    lateinit var tvTitleWorld: TextView
    lateinit var tvTitleGlobalConfirm: TextView
    lateinit var tvGolobalTitleDeath: TextView
    lateinit var tvTitleGolobalRecoved: TextView
    var mContext:Context?=null
    lateinit var mChart: PieChart
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.world_stat, container, false)
        initialize(view)
        if (InternetConnctionCheck.verifyAvailableNetwork(requireActivity() as AppCompatActivity)) {
            getGobarData()
        }
        return view
    }

    private fun initialize( view: View) {
        mContext=requireContext()
        mChart = view.findViewById(R.id.pieChart)
        mChart?.setUsePercentValues(true)
        tvTotalConfirmed=view.findViewById(R.id.tvGlobalConfirm)
        tvTotalDeath=view.findViewById(R.id.tvGlobalDeath)
        tvTotalRecovred=view.findViewById(R.id.tvGlobalRecovered)
        tvTitleGolobalRecoved=view.findViewById(R.id.tvTitleGolobalRecoved)
        tvGolobalTitleDeath=view.findViewById(R.id.tvGolobalTitleDeath)
        tvTitleGlobalConfirm=view.findViewById(R.id.tvTitleGlobalConfirm)
        tvTitleWorld=view.findViewById(R.id.tvTitleWorld)

        val tyTitle = Typeface.createFromAsset(mContext?.assets, "font/Roboto-Medium.ttf")
        val tyData = Typeface.createFromAsset(mContext?.assets, "font/Roboto-Regular.ttf")
        tvTitleGolobalRecoved.setTypeface(tyTitle)
        tvGolobalTitleDeath.setTypeface(tyTitle)
        tvTitleGlobalConfirm.setTypeface(tyTitle)
        tvTitleWorld.setTypeface(tyTitle)
        tvTotalConfirmed.setTypeface(tyData)
        tvTotalDeath.setTypeface(tyData)
        tvTotalRecovred.setTypeface(tyData)
    }

    private fun getGobarData() {
        val destinationService: DestinationService =
            ServiceBuilder.builService(DestinationService::class.java)
        val worldCorona = destinationService.getWorldData()
        worldCorona.enqueue(object : Callback<WorldCorona> {
            override fun onFailure(call: Call<WorldCorona>, t: Throwable) {
                Log.e("error", t.message)
            }

            override fun onResponse(call: Call<WorldCorona>, response: Response<WorldCorona>) {
                var globBalCOrona: WorldCorona? = response.body()!!
                tvTotalRecovred.setText(""+globBalCOrona?.totalRecovered)
                tvTotalDeath.setText(""+globBalCOrona?.totalDeaths)
                tvTotalConfirmed.setText(""+globBalCOrona?.totalConfirmed)
                mChart?.setUsePercentValues(true)
                val desc: Description = Description()
                desc.text = "% Rate Chart"
                mChart?.description = desc
                var totalActive:Float= globBalCOrona?.totalRecovered?.toFloat()!!
                var totalDeath:Float= globBalCOrona?.totalDeaths?.toFloat()!!
                var totalConfirm:Float= globBalCOrona?.totalConfirmed?.toFloat()!!
                // val legend: Legend? = requireActivity().legend
                val legend: Legend? = mChart?.legend
                legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT

                val value = Arrays.asList(totalConfirm, totalDeath, totalActive)
                val label = Arrays.asList("Confirm", "Dead", "Recovered")

                val entry = ArrayList<PieEntry>()
                for (i in value.indices) {
                    value.get(i)?.let { PieEntry(it, label.get(i)) }?.let { entry.add(it) }

                }


                val dataSet = PieDataSet(entry, "")
                dataSet.setColors(Color.RED, Color.BLACK, Color.GREEN);
                dataSet.setDrawValues(true)
                //  dataSet.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);

                val pieData = PieData(dataSet)
                pieData.setValueFormatter(PercentFormatter())
                pieData.setValueTextSize(10f)
                pieData.setValueTextColor(Color.WHITE)

                mChart?.data = pieData
            }

        })
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String) =
            WorldData().apply {
                arguments = Bundle().apply {
                    putString("World", "World")

                }
            }
    }
}